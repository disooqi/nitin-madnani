//^\d+\s
#include "Prob.h"
#include "Ngram.h"
#include "Vocab.h"
#include "srilm.h"
#include <cstdio>
#include <cstring>
#include <cmath>

Vocab *swig_srilm_vocab;

/* Initialize the ngram model */
Ngram* initLM(int order) {
swig_srilm_vocab = new Vocab;
return new Ngram(*swig_srilm_vocab, order);
}

/* Delete the ngram model */
void deleteLM(Ngram* ngram) {
delete swig_srilm_vocab;
delete ngram;
}

/* Read the given LM file into the model */
int readLM(Ngram* ngram, const char* filename) {
File file(filename, "r");
if (!file) {
fprintf(stderr,"Error:: Could not open file %s\n", filename);
return 0;
}
else return ngram->read(file, 0);
}

/* How many ngrams are there? */
int howManyNgrams(Ngram* ngram, unsigned order) {
return ngram->numNgrams(order);
}

/* Get the probability and perplexity values for a given sentence */
void getSentenceProbPpl(Ngram* ngram, char* sentence, float ans[3]) {
char* words[80];
unsigned numparsed;
TextStats stats;
char* scp;

/* Create a copy of the input string to be safe */
scp = strdupa(sentence);

/* Parse the sentence into its constitutent words */
numparsed = Vocab::parseWords(scp, (VocabString *)words, 80);
if (numparsed > 80) {
fprintf(stderr, "Error: Number of words in sentence should be <= 80.\n");
}

/* Calculate the sentence probability and store it in stats */
ans[0] = ngram->sentenceProb(words, stats);

/* Now calculate the denominator for perplexity */
if (stats.numWords + stats.numSentences > 0) {
int denom = stats.numWords - stats.numOOVs - stats.zeroProbs + 1;

/* Calculate ppl value */
if (denom > 0)
ans[1] = LogPtoPPL(stats.prob/denom);
else
ans[1] = -1.0;

/* calculate ppl1 value */
denom -= 1;
if (denom > 0)
ans[2] = LogPtoPPL(stats.prob/denom);
else
ans[2] = -1.0;
}
}

/* Run the file through SRILM and store its statistics into a TextStats instance */
unsigned fileStats(Ngram* ngram, const char* filename, TextStats &stats) {
File givenfile(filename, "r");
if (!givenfile) {
fprintf(stderr,"Error:: Could not open file %s\n", filename);
return 1;
}
else {
ngram->pplFile(givenfile, stats, 0);
return 0;
}
}

/* Compute probability and perplexity over entire file */
void getFileProbPpl(Ngram* ngram, const char* filename, float ans[3]) {
TextStats stats;

if (!fileStats(ngram, filename, stats)) {

/* calculate the file logprob */
ans[0] = stats.prob;

/* calculate the perplexity values if we can */
if (stats.numWords + stats.numSentences > 0) {
int denom = stats.numWords - stats.numOOVs - stats.zeroProbs + stats.numSentences;

/* calculate ppl value */
if (denom > 0)
ans[1] = LogPtoPPL(stats.prob/denom);
else
ans[1] = -1.0;

/* calculate ppl1 value */
denom -= stats.numSentences;
if (denom > 0)
ans[2] = LogPtoPPL(stats.prob/denom);
else
ans[2] = -1.0;
}
}
}
/* Generate random sentences using the language model */
void randomSentences(Ngram* ngram, unsigned numSentences, const char* filename) {
VocabString* sent;
File outFile(filename, "w");
unsigned i;

/* set seed so that new sentences are generated each time */
srand48(time(NULL) + getpid());

for (i = 0; i < numSentences; i++) {
/* call the built-in SRILM method to generate a random sentence */
sent = ngram->generateSentence(50000, (VocabString *) 0);

/* write the generated sentence to the file */
swig_srilm_vocab->write(outFile, sent);
fprintf(outFile, "\n");
}
outFile.close();
}