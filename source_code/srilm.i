/* We want our Python module to be called �srilm� */
%module srilm

/* Include all needed header files here */
%{
#include "Ngram.h"
#include "Vocab.h"
#include "Prob.h"
#include "srilm.h"
%}

/* We need to generate interfaces for all functions defined in srilm.h */
%include srilm.h

/* We need a C to Python array bridge and SWIG comes with one */
%include "carrays.i"
%array_class(float, floatArray);

/* Since we are allocating memory when creating random sentences */
/* make sure that we tell SWIG so that it can free it */
%newobject randomSentences;