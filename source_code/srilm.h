#ifndef SRILMWRAP_H
#define SRILMWRAP_H

extern "C" {
Ngram* initLM(int order);
void deleteLM(Ngram* ngram);
int readLM(Ngram* ngram, const char* filename);
int howManyNgrams(Ngram* ngram, unsigned order);
void getSentenceProbPpl(Ngram* ngram, char* sentence, float ans[3]);
unsigned fileStats(Ngram* ngram, const char* filename, TextStats &stats);
void getFileProbPpl(Ngram* ngram, const char* filename, float ans[3]);
void randomSentences(Ngram* ngram, unsigned numSentences, const char* filename);
}

#endif